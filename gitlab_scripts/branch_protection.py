import os
import requests

# GitLab API endpoint and your personal access token
GITLAB_URL = 'https://gitlab.com/api/v4/'
PRIVATE_TOKEN = os.environ.get('GITLAB_GROUP_ACCESS_TOKEN')

# Root group ID and default branch names
ROOT_GROUP_ID = int(os.environ.get('GITLAB_GROUP_ID', 0))
DEFAULT_BRANCH_NAMES = os.environ.get('DEFAULT_BRANCH_NAMES', 'master').split(',')

# Branch protection rules to be applied
BRANCH_PROTECTION_RULES = {
    "allow_force_push": False,
    "allowed_to_push": {
        "access_level": 0,
        "users": [],
        "groups": []
    },
    "allowed_to_merge": {
        "access_level": 30,
        "users": [],
        "groups": []
    }
}

def get_subgroups(parent_group_id):
    url = f"{GITLAB_URL}groups/{parent_group_id}/subgroups"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to get subgroups for group ID {parent_group_id}")
        return []

def get_group_projects(group_id):
    url = f"{GITLAB_URL}groups/{group_id}/projects"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to get projects for group ID {group_id}")
        return []

def protect_default_branch(project_id, branch_name):
    url = f"{GITLAB_URL}projects/{project_id}/protected_branches/{branch_name}"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    data = {
        "name": branch_name,
        "push_access_level": BRANCH_PROTECTION_RULES['allowed_to_push']['access_level'],
        "merge_access_level": BRANCH_PROTECTION_RULES['allowed_to_merge']['access_level'],
        "unprotect_access_level": 40
    }

    # Try to delete the existing protection
    response = requests.delete(url, headers=headers)

    if response.status_code == 204:
        print(f"Deleted existing branch protection for project ID {project_id}, branch {branch_name}")

    # Apply the new protection
    response = requests.post(f"{GITLAB_URL}projects/{project_id}/protected_branches", headers=headers, data=data)
    if response.status_code == 201:
        print(f"Protected branch {branch_name} in project ID {project_id}")
    else:
        print(f"Failed to protect branch {branch_name} in project ID {project_id}")
        print(f"Error message: {response.json()}")

def process_group(group_id):
    # Process subgroups
    subgroups = get_subgroups(group_id)
    for subgroup in subgroups:
        subgroup_id = subgroup['id']
        process_group(subgroup_id)

    # Process projects
    projects = get_group_projects(group_id)
    for project in projects:
        project_id = project['id']
        print(f"Processing project ID: {project_id}")
        for branch_name in DEFAULT_BRANCH_NAMES:
            protect_default_branch(project_id, branch_name)

def main():
    # Process the root group
    process_group(ROOT_GROUP_ID)

if __name__ == "__main__":
    main()
